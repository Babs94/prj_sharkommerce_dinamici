from django import template

register = template.Library()

@register.filter
def multiply(op1, op2):
    return op1 * op2