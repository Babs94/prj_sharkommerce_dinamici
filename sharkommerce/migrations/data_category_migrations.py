from django.db import migrations

categories = [
    ("pesci", ["ciprinidi", "anabantidi", "poecilidi"]),
    ("coralli", ["molli", "LPS", "SPS"]),
    ("vasche", ["aperte", "chiuse"]),
    ("tecnica", ["filtri", "luci", "mangiatoie"]),
    ("piante", ["epifite", "stelo", "muschi"]),
    ("cibo", ["secco", "vivo", "congelato"])
]

def combine_names(apps, schema_editor):
    Category = apps.get_model('sharkommerce', 'Category')

    for cat, subcats in categories:
        Category.objects.bulk_create([
            Category(cat_name=cat, subcat_name=subcat)
            for subcat in subcats
        ])

class Migration(migrations.Migration):

    dependencies = [
        ('sharkommerce', '0001_initial'),
    ]

    operations = [
        migrations.RunPython(combine_names),
    ]
