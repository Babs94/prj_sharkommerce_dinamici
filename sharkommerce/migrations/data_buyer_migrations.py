from django.db import migrations
from django.contrib.auth.hashers import make_password

buyers = [
    {"user_data": {"username": "betta_lover", "email": "bettalover81@mail.it", "password": make_password("betta_lover")}, "address": "via della pace 4, Firenze, 50100, Italia", "birth_date": "1981-07-21"},
    {"user_data": {"username": "nao93", "email": "nao93@mail.com", "password": make_password("nao93")}, "address": "piazza della Libertà, Bologna, 40121, Italia", "birth_date": "1993-01-16"},
    {"user_data": {"username": "giovanni_nucci", "email": "gnucci@mail.com", "password": make_password("giovanni_nucci")}, "address": "via Carducci, Catania, 95100, Italia", "birth_date": "1952-07-21"},
]


def create_buyer(apps, schema_editor):
    Buyer = apps.get_model('sharkommerce', 'Buyer')
    User = apps.get_model('auth', 'User')

    for buyer in buyers:
        u = User(**buyer["user_data"])
        b = Buyer(user=u, address=buyer["address"], birth_date=buyer["birth_date"])
        u.save()
        b.save()



class Migration(migrations.Migration):

    dependencies = [
        ('sharkommerce', 'data_seller_migrations'),
    ]

    operations = [
        migrations.RunPython(create_buyer),
    ]