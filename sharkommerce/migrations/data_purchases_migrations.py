from django.db import migrations

purchases = [
    {"buyer":"betta_lover", "product": {"seller": "mondomarino_lab", "item": "Danio Margaritatus"}, "qty": 10, "date": "2020-10-09", "price": 3.50},
    {"buyer": "betta_lover", "product": {"seller": "sealife_shop", "item": "Java Moss"}, "qty": 1,
     "date": "2020-10-09", "price": 2.50},
    {"buyer": "nao93", "product": {"seller": "sealife_shop", "item": "Anemone"}, "qty": 2,
     "date": "2020-01-29", "price": 20.00},
    {"buyer": "giovanni_nucci", "product": {"seller": "asiatic_world", "item": "Trichopodus leerii"}, "qty": 2,
     "date": "2020-01-19", "price": 20.50}
]


def create_purchases(apps, schema_editor):
    Seller = apps.get_model('sharkommerce', 'Seller')
    Product = apps.get_model('sharkommerce', 'Product')
    Purchase = apps.get_model('sharkommerce', "Purchase")
    Buyer = apps.get_model("sharkommerce", "Buyer")

    for elem in purchases:
        prod = Product.objects.get(seller=Seller.objects.get(user__username=elem["product"]["seller"]),
                                                          name=elem["product"]["item"])
        purchased_qty=elem["qty"]
        if purchased_qty <= prod.qty: #in realtà non dovrei avere bisogno di questo controllo, ma simula quello che viene fatto all'inserimento di un acquisto
            prod.qty -= purchased_qty
            prod.save()
            new_purch = Purchase(buyer=Buyer.objects.get(user__username=elem["buyer"]), product=prod, qty=purchased_qty,
                                 date=elem["date"], price=elem["price"])
            new_purch.save()
        else:
            raise ValueError("Not enough items in stock for element ", elem["product"]["item"])


class Migration(migrations.Migration):
    dependencies = [
        ('sharkommerce', '0001_auto_20201227_1610'),#in realtà questa avrà come dipendenza data_products_migrations
    ]

    operations = [
        migrations.RunPython(create_purchases),
    ]
