from django.db import migrations

seller_reviews = [
    {'seller':'mondomarino_lab','buyer': 'betta_lover','date':"2020-10-10",'vote':10,'title':'Massima disponibilita','description':'Ho contattato il venditore per avere alcune informazioni sulle condizioni migliori per i pesci che stavo acquistando, e si è dimostrato davvero gentile, competente e disponibile.'},
    {'seller': 'sealife_shop', 'buyer': 'betta_lover', 'date': "2020-10-15", 'vote': 8,
     'title': 'Tutto in ordine, peccato per la consegna',
     'description': 'Acquisto consegnato con la massima cura, ma il corriere utilizzato non ha rispettato i tempi di consegna.'},
    {'seller': 'sealife_shop', 'buyer': 'nao93', 'date': "2020-01-30", 'vote': 9,
     'title': 'Soddisfatto','description': 'Ordine in buone condizioni e buona qualità.'},
    {'seller':'asiatic_world','buyer': 'giovanni_nucci','date':'2020-01-21','vote':5,'title':'C\'è di meglio','description':'Visto il costo e le condizioni in cui sono arrivati gli animali, penso si possa trovare di meglio in giro.'}

]


def create_seller_reviews(apps, schema_editor):
    Seller = apps.get_model('sharkommerce', 'Seller')
    Buyer = apps.get_model("sharkommerce", "Buyer")
    SellerReview = apps.get_model('sharkommerce','SellerReview')
    Purchase = apps.get_model('sharkommerce','Purchase')
    Product = apps.get_model('sharkommerce','Product')

    for elem in seller_reviews: # vuoi metterla in bulk?
        sel = Seller.objects.get(user__username=elem["seller"])
        buy = Buyer.objects.get(user__username=elem["buyer"])
        if Purchase.objects.filter(buyer=buy,product__in=Product.objects.filter(seller=sel)):  # se vuoi, aggiungi il controllo sulle date e un else

            review = SellerReview(seller=sel, buyer=buy, date=elem["date"],vote=elem["vote"], title=elem["title"],
                                  description=elem["description"])
            review.save()


class Migration(migrations.Migration):
    dependencies = [
        ('sharkommerce', '0001_auto_20201228_1524'),# questa dipenderà dalle purchase
    ]

    operations = [
        migrations.RunPython(create_seller_reviews),
    ]
