from django.db import migrations

purchases = [
    {"buyer":"nao93", "product": {"seller": "mondomarino_lab", "item": "Danio Margaritatus"}, "qty": 2, "date": "2021-01-07", "price": 3.50},
    {"buyer": "nao93", "product": {"seller": "asiatic_world", "item": "Mangime"}, "qty": 1,
     "date": "2021-01-07", "price": 3.80}
]


def create_purchases(apps, schema_editor):
    Seller = apps.get_model('sharkommerce', 'Seller')
    Product = apps.get_model('sharkommerce', 'Product')
    Purchase = apps.get_model('sharkommerce', "Purchase")
    Buyer = apps.get_model("sharkommerce", "Buyer")

    for elem in purchases:
        prod = Product.objects.get(seller=Seller.objects.get(user__username=elem["product"]["seller"]),
                                                          name=elem["product"]["item"])
        purchased_qty=elem["qty"]
        if purchased_qty <= prod.qty: #in realtà non dovrei avere bisogno di questo controllo, ma simula quello che viene fatto all'inserimento di un acquisto
            prod.qty -= purchased_qty
            prod.save()
            new_purch = Purchase(buyer=Buyer.objects.get(user__username=elem["buyer"]), product=prod, qty=purchased_qty,
                                 date=elem["date"], price=elem["price"])
            new_purch.save()
        else:
            raise ValueError("Not enough items in stock for element ", elem["product"]["item"])


class Migration(migrations.Migration):
    dependencies = [
        ('sharkommerce', 'data_purchases4_prod_reviews_test'),#in realtà questa avrà come dipendenza data_products_migrations
    ]

    operations = [
        migrations.RunPython(create_purchases),
    ]
