from django.db import migrations
from django.contrib.auth.hashers import make_password

sellers = [
    {"user_data": {"username": "mondomarino_lab", "email": "mondomarino@mail.it", "password": make_password("mondomarino")}, "store": "Mondomarino"},
    {"user_data": {"username": "sealife_shop", "email": "sealife@mail.com", "password": make_password("sealife")}, "store": "Sealife"},
    {"user_data": {"username": "asiatic_world", "email": "asiatic_world@mail.com", "password": make_password("asiatic-world")}, "store": "Asiatic World"},
]


def create_seller(apps, schema_editor):
    Seller = apps.get_model('sharkommerce', 'Seller')
    User = apps.get_model('auth', 'User')

    for seller in sellers:
        u = User(**seller["user_data"])
        s = Seller(user=u, store=seller["store"])
        u.save()
        s.save()



class Migration(migrations.Migration):

    dependencies = [
        ('sharkommerce', 'data_category_migrations'),
    ]

    operations = [
        migrations.RunPython(create_seller),
    ]