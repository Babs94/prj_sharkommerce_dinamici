import os

from django.db import migrations
from django.core.files.storage import default_storage
from sharkommerce.models import get_random_string
from django.conf import settings

products = [
    {"seller": "mondomarino_lab", "categories": {"cat":"pesci","subcat": "ciprinidi"}, "name": "Danio Margaritatus",
     "image": os.path.join("immagini_default_inserimento_prodotti","Danio Margaritatus.jpg"), "qty":36, "price":3.50,
     "description": "Pesce dal temperamento timido, adatto ad essere allevato in piccoli gruppi, raggiunge al massimo "
                    "una dimensione di circa 3 cm"},
    {"seller": "mondomarino_lab", "categories":{"cat":"pesci","subcat": "poecilidi"}, "name": "Lebistes",
     "image": os.path.join("immagini_default_inserimento_prodotti", "lebistes.jpg"), "qty":50, "price":2.50,
     "description": "Pesci colorati d'acque dure, adatti ad essere allevato in gruppi, età circa 2 mesi"},
    {"seller": "mondomarino_lab", "categories": {"cat":"piante", "subcat":"stelo"}, "name": "Althernanthera",
     "image": os.path.join("immagini_default_inserimento_prodotti","althernanthera.jpg"), "qty": 5, "price": 5.60,
     "description": "Pianta rossa perfetta per spazi molto illuminati, è consigliato l'utlizzo di co2"},
    {"seller": "mondomarino_lab", "categories": {"cat":"tecnica","subcat": "filtri"}, "name": "Filtro Esterno",
     "image": os.path.join("immagini_default_inserimento_prodotti","filtro_esterno_150.jpg"), "qty":3, "price":80.50,
     "description": "Filtro esterno per acquari fino a 150l, estremamente silenzioso"},
    {"seller": "mondomarino_lab", "categories": {"cat":"vasche","subcat": "aperte"}, "name": "Acquario Cubic",
     "image": os.path.join("immagini_default_inserimento_prodotti","open_tank_40l.jpg"), "qty":4, "price":50.50,
     "description": "Piccolo cubo elegante, adatto all'allevamento di caridine, nanoreef o pesci di piccola dimensione"},
    {"seller": "mondomarino_lab", "categories": {"cat":"coralli","subcat": "molli"}, "name": "Anemone",
     "image": os.path.join("immagini_default_inserimento_prodotti","coralli_molli_anemone2.jpg"), "qty":4, "price":19.50,
     "description": "Anemoni allevati presso di noi, di un bel colore brillante."},
    {"seller": "mondomarino_lab", "categories":{"cat": "coralli","subcat": "SPS"}, "name": "Montipora",
     "image": os.path.join("immagini_default_inserimento_prodotti", "montipora_sps.jpg"), "qty": 4, "price": 17.50,
     "description": "Anemoni per cui è consigliata esperienza, adatti a vasche mature e stabili"},

    {"seller": "sealife_shop", "categories": {"cat":"vasche", "subcat":"chiuse"}, "name": "Acquario OceanDream",
     "image": os.path.join("immagini_default_inserimento_prodotti","100l_closed.jpg"), "qty":6, "price":300.00,
     "description": "Vasca media adatta per principianti ed esperti, arriva già compresa di luci e filtro."},
    {"seller": "sealife_shop", "categories":{ "cat":"piante","subcat": "epifite"}, "name": "Anubias",
     "image": os.path.join("immagini_default_inserimento_prodotti","anubias.jpg"), "qty":6, "price": 4.00,
     "description": "Pianta epifita che richiede poca luce e pochi fertilizzanti."},
    {"seller": "sealife_shop", "categories": {"cat":"coralli","subcat": "molli"}, "name": "Anemone",
     "image": os.path.join("immagini_default_inserimento_prodotti","coralli_molli_anemone.jpg"), "qty": 5, "price":20.00,
     "description": "Anemone perfetto per l'allevamento in acquario salato"},
    {"seller": "sealife_shop", "categories": {"cat":"coralli","subcat": "LPS"}, "name": "Acanthastrea",
     "image": os.path.join("immagini_default_inserimento_prodotti","lps_acanthastrea.jpg"), "qty": 7, "price":12.00,
     "description": "Corallo LPS appena importato dal nostro fornitore di fiducia, ottimi esemplari"},
    {"seller": "sealife_shop", "categories": {"cat":"coralli","subcat": "SPS"}, "name": "Stylophora",
     "image": os.path.join("immagini_default_inserimento_prodotti","sps_stylophora.jpg"), "qty": 5, "price":13.00,
     "description": "Corallo adatto per l'allevamento di appassionati esperti"},
    {"seller": "sealife_shop", "categories": {"cat":"piante","subcat": "muschi"}, "name": "Java Moss",
     "image": os.path.join("immagini_default_inserimento_prodotti","java_moss.jpg"), "qty": 15, "price": 2.50,
     "description": "Muschio ottimo per creare nascondigli per caridine e pesci"},
    {"seller": "sealife_shop", "categories": {"cat":"pesci","subcat": "poecilidi"}, "name": "Lebistes",
     "image": os.path.join("immagini_default_inserimento_prodotti","lebistes_2.jpg"), "qty": 50, "price":2.00,
     "description": "Pesci vivaci, allevati da noi, garantiamo la purezza della specie"},

    {"seller": "asiatic_world", "categories": {"cat":"pesci","subcat": "ciprinidi"}, "name": "Danio Margaritatus",
     "image": os.path.join("immagini_default_inserimento_prodotti", "Danio_Margaritatus2.jpg"), "qty": 25, "price":4.50,
     "description": "Vivaci esemplari riprodotti ed allevati presso la nostra struttura"},
    {"seller": "asiatic_world", "categories": {"cat": "piante","subcat": "stelo"}, "name": "Ceratophyllum",
     "image": os.path.join("immagini_default_inserimento_prodotti","ceratophyllum.jpg"), "qty": 15, "price": 3.50,
     "description": "Pianta molto resistente adatta sia alla coltivazione in immersione che come emersa"},
    {"seller": "asiatic_world", "categories": {"cat":"pesci","subcat": "anabantidi"}, "name": "Colisa Lalia",
     "image": os.path.join("immagini_default_inserimento_prodotti","colisa_lalia.jpg"), "qty": 10, "price": 7.50,
     "description": "Esemplari in buona salute, importati, di circa 5 mesi"},
    {"seller": "asiatic_world", "categories": {"cat":"piante","subcat": "stelo"}, "name": "Ceratophyllum",
     "image": os.path.join("immagini_default_inserimento_prodotti","ceratophyllum.jpg"), "qty": 15, "price": 3.50,
     "description": "Pianta molto resistente adatta sia alla coltivazione in immersione che come emersa"},
    {"seller": "asiatic_world", "categories": {"cat": "tecnica","subcat": "filtri"}, "name": "Filtro Interno",
     "image": os.path.join("immagini_default_inserimento_prodotti","filtro_interno.jpg"), "qty": 5, "price": 20.50,
     "description": "Filtro per piccole vasche fino a 40l"},
    {"seller": "asiatic_world", "categories": {"cat":"vasche","subcat": "aperte"}, "name": "Acquario BluRiver",
     "image": os.path.join("immagini_default_inserimento_prodotti","open_tank_80l.jpg"), "qty": 8, "price": 150.50,
     "description": "Vasca aperta di 80l lordi, con filtro e luci già incluse"},
    {"seller": "asiatic_world", "categories": {"cat":"pesci","subcat": "anabantidi"}, "name": "Trichopodus leerii",
     "image": os.path.join("immagini_default_inserimento_prodotti","pearl_gourami.jpg"), "qty": 5, "price": 20.50,
     "description": "Chiamato anche pearled gourami per la caratteristica livrea, è un pesce colorato e caratteristico"
                    " che può raggiungere i 10-13 cm "},
    {"seller": "asiatic_world", "categories": {"cat":"cibo","subcat": "secco"}, "name": "Mangime",
     "image": os.path.join("immagini_default_inserimento_prodotti","secco.jpg"), "qty": 13, "price": 3.80,
     "description": "Mangime secco completo per la dieta di pesci onnivori di medie dimensioni"},

]


def insert_prod(apps, schema_editor):
    Product = apps.get_model('sharkommerce', 'Product')
    Seller = apps.get_model('sharkommerce', 'Seller')
    Category = apps.get_model('sharkommerce', 'Category')

    prods = []

    for prod in products:
        with default_storage.open(prod["image"]) as img:
            image_path = os.path.join(prod["seller"], f"img_{get_random_string(length=16)}")
            default_storage.save(image_path, img)
            prods.append(Product(seller=Seller.objects.get(user__username=prod["seller"]),
                        categories=Category.objects.get(cat_name=prod["categories"]["cat"], subcat_name=prod["categories"]["subcat"]),
                        name=prod["name"], image=image_path, qty=prod["qty"], price=prod["price"], description=prod["description"]))
    Product.objects.bulk_create(prods)

class Migration(migrations.Migration):

    dependencies = [
        ('sharkommerce', 'data_buyer_migrations'),
    ]

    operations = [
        migrations.RunPython(insert_prod),
    ]


