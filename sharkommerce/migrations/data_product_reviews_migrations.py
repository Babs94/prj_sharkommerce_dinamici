from django.db import migrations

product_reviews = [
    #sarebbe più corretto utilizzare i numeri dei prodotti, ma al momento conosciamo i record inseriti e sappiamo che il nome in combinazione con la data e l'acquirente sono
    #sufficienti per avere l'univocità
    {'purchase':{'product':'Filtro Esterno', 'date': '2020-11-09', 'buyer':'betta_lover'},'vote':9,
     'title':'Filtro funzionale','description':'Filtro silenzioso e prestante', 'date':'2020-11-10'},
    {'purchase':{'product':'Danio Margaritatus', 'date' : '2020-11-09', 'buyer':'giovanni_nucci'},'vote':8,
     'title':'Pesci in salute','description':'Pesci arrivati in ottima salute, con colori brillanti', 'date':'2020-11-12'},

]


def create_seller_reviews(apps, schema_editor):
    ProductReview = apps.get_model('sharkommerce','ProductReview')
    Purchase = apps.get_model('sharkommerce','Purchase')

    for elem in product_reviews: # vuoi metterla in bulk?
        purch = Purchase.objects.get(product__name=elem["purchase"]["product"], date=elem["purchase"]["date"], buyer__user__username=elem["purchase"]["buyer"]) # vuoi gestire anche se non esiste?
        review = ProductReview(purchase=purch, vote=elem["vote"],title=elem["title"],description=elem["description"],date=elem["date"])
        review.save()


class Migration(migrations.Migration):
    dependencies = [
        ('sharkommerce', '0002_productreview_date'),# questa dipenderà dalle data_purchases_3
    ]

    operations = [
        migrations.RunPython(create_seller_reviews),
    ]
