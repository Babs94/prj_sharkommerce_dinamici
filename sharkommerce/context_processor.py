from .models import *

#this is shared among different views ( home, register, login... )
def base_context(request):
    cat = Category.objects.all()
    categories = {}
    for elem in cat:
        if elem.cat_name not in categories:
            categories[elem.cat_name] = {}
            if elem.subcat_name not in categories[elem.cat_name]:
                categories[elem.cat_name] = {}
        categories[elem.cat_name][elem.subcat_name] = elem.id



    context = {'categories': categories}
    return context



