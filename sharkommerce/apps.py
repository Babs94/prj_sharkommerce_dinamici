from django.apps import AppConfig


class SharkommerceConfig(AppConfig):
    name = 'sharkommerce'
