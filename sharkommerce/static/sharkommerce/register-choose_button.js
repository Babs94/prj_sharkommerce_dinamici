
const negozio = document.getElementById("id_negozio");
const data_nascita = document.getElementById("id_data_nascita");
const indirizzo = document.getElementById("id_indirizzo");
document.getElementById("id_tipo_0").addEventListener("change", changeType, false);
document.getElementById("id_tipo_1").addEventListener("change", changeType, false);

function changeType() {
	if (document.getElementById("id_tipo_1").checked) { //seller
		negozio.style.display = "Block";
		negozio.getElementsByTagName('input')[0].required = true;
		data_nascita.style.display = "None";
		data_nascita.getElementsByTagName('input')[0].required = false;
		indirizzo.style.display ="None";
		indirizzo.getElementsByTagName('input')[0].required = false;

	} else { //buyer
		negozio.style.display = "None";
		negozio.getElementsByTagName('input')[0].required = false;
		data_nascita.style.display = "Block";
		data_nascita.getElementsByTagName('input')[0].required = true;
		indirizzo.style.display="Block";
		indirizzo.getElementsByTagName('input')[0].required = true;
	}

}
    changeType();
