category = document.getElementById("category-box")
subcat = document.getElementById("subcat_box")
product = document.getElementById("id_product")
imageField = document.getElementById("id_image")
imgPreview = document.getElementById("img_preview")
checkNewProd = document.getElementById("id_check_new_product")
newProd = document.getElementById("id_new_product")

imageField.addEventListener('change', (event) => {
    let file = event.target.files[0] //riferimento al file selezionato

    if (file.type && file.type.indexOf('image') === -1) {
        console.log("File is not an image.", file.type, file)
        return
    }

    const reader = new FileReader()
    reader.addEventListener('load', (event) => { //asincrono
        imgPreview.src = event.target.result //visualizzatore dell'immagine
    })
    reader.readAsDataURL(file)
})

function updateSubcats() {
    selected_cat = $(category).find("option:selected").text()
    subcats = categories[selected_cat]

    let items = subcat.length
    for (let i=0; i<items; i++)
        subcat.remove(0)

    for (i=0; i<subcats.length; i++){
        tag = document.createElement("option")
        tag.innerHTML = subcats[i]
        subcat.append(tag)
    }

    updateProducts()
}

/* questa funzione fa una chiamata get alla view product_cat che restituisce i prodotti corrispondenti alla categoria scelta */
var getJSON = function(url, callback) {
    var xhr = new XMLHttpRequest()
    xhr.open('GET', url, true)
    xhr.responseType = 'json'
    xhr.onload = function() {
        var status = xhr.status
        if (status === 200) {
            callback(null, xhr.response)
        } else {
            callback(status, xhr.response)
        }
    }
    xhr.send()
}

/*questa funzione aggiorna la combo box dei prodotti in base alla category selezionata*/
function updateProducts() {
    var cat_name = category.value
    var subcat_name = subcat.value
    getJSON("/products/" + cat_name + "/" + subcat_name, (err, data) => {
        if (err != null) {
            console.log("ERROR CODE: " + err)
        } else {
            var new_products = data["products"]
            var items = product.length /*questo pezzo si può unire a quello di updateSubcats*/
            for (let i=0; i<items; i++)
                product.remove(0)

            for (i=0; i<new_products.length; i++){
                tag = document.createElement("option")
                tag.innerHTML = new_products[i]
                product.append(tag)
            }
        }
    })
}

function activateNewProd() {
    if (checkNewProd.checked) {
        product.setAttribute("disabled", null)
    }else {
        //pulisci la newProd
        product.removeAttribute("disabled")
    }
}

updateSubcats()
updateProducts()