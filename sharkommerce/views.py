
import datetime
from django.contrib import messages
from django.contrib.auth.models import User
from django.shortcuts import render, get_object_or_404, reverse
from sharkommerce.Utils.SellerProfileUtils import *
from sharkommerce.Utils.ItemUtils import *
from sharkommerce.forms_class import RegisterForm, LoginForm, ProductForm, ReviewForm, FiltersForm, ItemForm, \
    ProductReviewForm
from .models import Category, Seller, Buyer, Product, Purchase, SellerReview, ProductReview
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from django.views.decorators.http import require_GET
from django.http import HttpResponse, HttpResponseRedirect, JsonResponse



def index(request):
    try:
        is_seller = Seller.objects.get(user_id=request.user.id)
    except Seller.DoesNotExist:
        is_seller = False

    return render(request, 'sharkommerce/homefile.html',{'is_seller': is_seller})

#questa view rimuove gli oggetti dal carrello e fa redirect alla view del carrello
def removeFromCart(request, product_id):
    del request.session["cart"][str(product_id)] # devo inserire il controllo sul fatto che esista nel carrello?
    request.session.save()
    return HttpResponseRedirect(reverse('sharkommerce:cart'))


# controllo che il numero di prodotti inseriti nel carrello sia effettivamente acquistabile ( un utente potrebbe
# aggiungere al carrello più prodotti di quelli disponibili, sommandone ad es il massimo possibile dello stock in più
# giri
def check_qty_available(items, cart):
    for prod in items:
        if prod.qty < cart[str(prod.id)]:
            return False
    return True


@login_required
def purchase(request):
    if not Buyer.objects.filter(user=request.user).exists():
        messages.error(request, '*puoi effettuare acquisti solo con un profilo da acquirente*')
        return HttpResponseRedirect(reverse('sharkommerce:login'))
    if request.method == 'POST':
        try:
            if request.session["cart"]:
                items = Product.objects.filter(id__in=request.session["cart"])
                if check_qty_available(items, request.session["cart"]):
                    purch = [Purchase(buyer_id=request.user.id, product=i, qty=request.session["cart"][str(i.id)],
                                      date=datetime.date.today(), price=i.price) for i in items]
                    for p in items:
                        p.update_qty(request.session["cart"][str(p.id)])
                    try:
                        Purchase.objects.bulk_create(purch)
                        request.session.flush()
                        messages.success(request, 'Acquisto inserito')
                        return HttpResponseRedirect(reverse('sharkommerce:profile', args=[request.user.id]))
                    except IntegrityError:
                        messages.error(request, '* Errore: prova più tardi *')
                else:
                    messages.error(request, '*troppi oggetti nel carrello rispetto la disponibilità*')
        except KeyError:
            request.session["cart"] = {}
            messages.error(request, '* Carrello vuoto *')
    return HttpResponseRedirect(reverse('sharkommerce:cart'))


@require_GET
def cart(request):
    items = []
    tot = 0
    try:
        if request.session["cart"]:
            items = Product.objects.filter(id__in=request.session["cart"])
        if len(items) > 0:
            for elem in items:
                tot += elem.price * request.session["cart"][str(elem.id)]
    except KeyError:
        request.session["cart"] = {}

    context = {'items': items, 'prod_qty': request.session["cart"], 'tot': tot, 'logged':request.user.is_authenticated} #passare la quantità così non mi convince

    return render(request, 'sharkommerce/cart.html', context)


#le funzioni utilizzate che non compaiono in questa classe sono sharkommerce.Utils.ItemUtils
def item(request, product_id):
    item = get_object_or_404(Product, pk=product_id)
    avg_rate = item.calculateAverageRate()  #è meglio se ha direttamente la colonna nel model; nel caso, mettilo in un try catch se non ha recensioni
    reviews = ProductReview.objects.filter(purchase__product=item)
    items_same_purch = get_items_in_same_purchase(item)
    recommended_items = []
    is_logged = request.user.is_authenticated
    # i candidati ideali per gli oggetti raccomandati sono gli oggetti più acquistati con l'oggetto che si sta guardando;
    # se non ce ne sono abbastanza si prendono gli oggetti della stessa categoria più acquistati ( o comunque oggetti
    # della stessa categoria, se non dovessero essercene di acquistati ). Se proprio non sono stati inseriti neanche
    # abbastanza oggetti della stessa categoria, vengono presi oggetti di altre categorie.
    for i in items_same_purch:
        recommended_items.append(Product.objects.get(id=i["product_id"]))
    if len(recommended_items) < 3:
        same_cat_items = get_items_same_category_more_bought(item)
        while len(recommended_items) != 3 and len(same_cat_items) != 0:
            recommended_items.append(Product.objects.get(id=same_cat_items.pop(0)['id']))
        if len(recommended_items) < 3:
            n_mancanti = 3-len(recommended_items)
            recommended_items = recommended_items + list(Product.objects.all().order_by('?')[:n_mancanti])
    item_form = ItemForm(max_qty = Product.objects.get(id=product_id).qty)
    context = {'item': item, 'avg_rate': avg_rate, 'reviews': reviews, 'recommended_items': recommended_items, 'item_form': item_form, 'is_logged':is_logged}
    if request.method == 'POST':

        item_form = ItemForm(request.POST, max_qty=Product.objects.get(id=product_id).qty)
        if item_form.is_valid():
            data = item_form.cleaned_data
            #request.session.flush()
            if "cart" not in request.session:
                #sarà popolato da id, qty per ogni prodotto aggiunto
                request.session["cart"] = {}
            # se l'id del prodotto non è presente nel carrello, viene aggiunto, con la rispettiva quantità; sennò, si
            # prende il suo valore e si somma alla nuova quantità. Il cast a str serve perchè dopo la save la chiave
            # viene trasformata in str anche se partiva come int.
            request.session["cart"][str(product_id)] = request.session["cart"].get(str(product_id), 0) + data["qty"]
            request.session.save()
        else:
            print("errore: quantità di prodotto selezionato errata")

    return render(request, 'sharkommerce/item.html', context)


def logout_view(request):
    logout(request)
    return HttpResponseRedirect(reverse('sharkommerce:index'))


def login_view(request):
    login_form = LoginForm()
    context = {'login_form': login_form}
    if request.method == 'POST':
        login_form = LoginForm(request.POST)
        if login_form.is_valid():
            data = login_form.cleaned_data
            user = authenticate(username=data['username'], password=data['password'])
            if user is not None:
                if user.is_active:
                    login(request, user)
                    next = request.GET.get("next", reverse('sharkommerce:index'))
                    return HttpResponseRedirect(next)
                else:
                    context.update({'errors': '* Your account is not enabled *'})  # controlla
            else:
                context.update({'errors': '* Username or password not correct *'})
        else:
            context.update({'errors': login_form.errors})  # qui non dovrei mai finirci


    return render(request, 'sharkommerce/login.html', context)


# this function prevents the user remove "required" from javascript code.
def check_is_really_compiled(data):
    if data["tipo"] == "Venditore":
        if data["negozio"] == "":
            return False
    if data["tipo"] == "Acquirente":
        if data["indirizzo"] == "":
            return False
        if data["data_nascita"] is None:
            return False
    return True


def register(request):  # da implementare : aggiunta ai gruppi
    register_form = RegisterForm()
    context = {'register_form': register_form}
    if request.method == 'POST':
        register_form = RegisterForm(request.POST)
        context.update({'register_form': register_form})
        if register_form.is_valid():
            data = register_form.cleaned_data
            if User.objects.filter(username=data['username']).exists():
                context.update({'errors': {'username': ['Username yet existant']}})
                return render(request, 'sharkommerce/register.html', context)
            if not check_is_really_compiled(data):
                context.update({'errors': {'last_forms': ['you miss a form']}})
                return render(request, 'sharkommerce/register.html', context)
            user = User.objects.create_user(username=data['username'], email=data['mail'], password=data['password'])
            if data['tipo'] == 'Venditore':
                venditore = Seller(user=user, store=data['negozio'])
                venditore.save()
            elif data['tipo'] == 'Acquirente':
                acquirente = Buyer(user=user, address=data['indirizzo'], birth_date=data['data_nascita'])
                acquirente.save()
            login(request, user)
            return HttpResponseRedirect(reverse('sharkommerce:index'))
        else:
            context.update({'errors': register_form.errors})
            return render(request, 'sharkommerce/register.html', context)
    else:
        return render(request, 'sharkommerce/register.html', context)


#controlla se chi sta visitando il profilo è il proprietario o no
def check_is_visitor_or_owner(request, id_visitor):
    if request.user == id_visitor:
        return True
    return False


def product_review(request):
    if request.method == "POST":
        product_review_form = ProductReviewForm(request.POST)
        if product_review_form.is_valid():
            data = product_review_form.cleaned_data
            prod = Purchase.objects.get(id=data["purch_id"]).product
            try:#controllo che non esista un'altra recensione con quel prodotto di quell'utente
                ProductReview.objects.get(purchase__buyer=request.user.id, purchase__product=prod )
            except ProductReview.DoesNotExist:
                review = ProductReview(purchase=Purchase.objects.get(id=data["purch_id"]), vote= data["vote"], title=data["title"], description=data["description"],  date=datetime.date.today())
                review.save()
            else:
                messages.error(request,"Errore: recensione già inserita per questo prodotto")
        else:
            print("not valid")

        return HttpResponseRedirect(reverse('sharkommerce:profile', args=[request.user.id]))


def buyer_profile(request, user_id):
    buyer = Buyer.objects.get(user=user_id)
    purchases = Purchase.objects.filter(buyer=buyer).order_by("-date")
    is_owner = check_is_visitor_or_owner(request, user_id)
    prod_reviews = ProductReview.objects.filter(purchase__buyer=buyer)
    sel_reviews = SellerReview.objects.filter(buyer=buyer)
    is_logged = request.user.is_authenticated
    prod_review_form = ProductReviewForm()

    context = {'buyer':buyer,'purchases':purchases, 'owner':is_owner,'prod_reviews':prod_reviews,
               'sel_reviews':sel_reviews, 'prod_review_form':prod_review_form, 'is_logged': is_logged}
    return render(request, 'sharkommerce/buyer_profile.html', context)


#le funzioni utilizzate che non sono presenti sono in sharkommerce.Utils.SellerProfileUtils o nei CommonUtils
def seller_profile(request, user):
    seller = Seller.objects.get(user=user)
    extracted_purchases = Purchase.objects.filter(product__seller=seller).order_by("-date")
    selling_prod = Product.objects.filter(seller=seller)
    reviews = SellerReview.objects.filter(seller=seller)
    is_logged = request.user.is_authenticated
    #ottieni la media per mese delle statistiche al venditore
    reviews_statistics = get_avg_rev_statistics(user)

    #ottieni le statistiche sugli acquisti fatti presso il venditore per mese/anno
    purch_statistics = get_tot_statistics(user)

    #assicurati che compaiano tutti i mesi ( per nome) , valorizzati
    purch_stat_months = get_tot_months(purch_statistics, reviews_statistics)

    #ottieni il totale del guadagno per anno
    purch_stat_years = get_tot_years(purch_stat_months)

    product_form = ProductForm()
    #controlla se c'è un controllo che segnali che non ci sono ancora prodotti in vendita se non ne ha ancora aggiunto nessuno
    context = {'seller': seller, 'purchases': extracted_purchases,
               "reviews": reviews, "products": selling_prod, "stat_years": purch_stat_years,
               "stat_months":purch_stat_months, "product_form": product_form, 'is_logged': is_logged}

    is_owner = check_is_visitor_or_owner(request, user)

    context["owner"] = is_owner #da qui, poi sistema sia il seller che il buyer

    try:
        Buyer.objects.get(user=request.user.id)
    except Buyer.DoesNotExist:
        type_visitor = "Other"
    else:
        type_visitor = "Buyer"
        context["seller_review_form"] = ReviewForm()
    context["type_visitor"] = type_visitor

    if request.method == 'POST':
        product_form = ProductForm(request.POST, request.FILES)
        context["product_form"] = product_form
        if product_form.is_valid():
            data = product_form.cleaned_data
            if (data["check_new_product"]): #andrò ad inserire un prodotto con un nome che non esisteva già
                # controllo che non ci siano stati utilizzi maliziosi della checkbox via javascript
                if not check_newProd_compiled(data["new_product"]):
                    context.update({'errors': {'select_box': ['Error in new product']}})
                    return render(request, 'sharkommerce/seller_profile.html', context)
                # controllo che non ci siano stati utilizzi maliziosi della select box via javascript
                #if not check_cat_subcat(data["category"], data["subcategory"]):
                if not check_category_subcategory(data["category"], data["subcategory"]):
                    context.update({'errors': {'select_box': ['Error in subcategories']}})
                    return render(request, 'sharkommerce/seller_profile.html', context)
                selected_cat = Category.objects.get(cat_name=data["category"], subcat_name=data["subcategory"])
                product = Product(seller=seller, categories=selected_cat, name=data["new_product"], image=data["image"],qty=data["quantity"], price = data["price"], description = data["description"])

            else:
                if not check_cat_subcat_prod(data["category"], data["subcategory"], data["product"]):# da testare
                    context.update({'errors': {'select_box': ['Error in subcategories or product box']}})
                    return render(request, 'sharkommerce/seller_profile.html', context)
                selected_cat = Category.objects.get(cat_name=data["category"], subcat_name=data["subcategory"])
                product = Product(seller=seller, categories=selected_cat, name=data["product"], image=data["image"],
                                  qty=data["quantity"], price=data["price"], description=data["description"])

            product.save()
            return HttpResponseRedirect(reverse('sharkommerce:profile', args=[user.id]))
        else:
            print(context["product_form"].errors) #sarà mai usato o lo tolgo?
    return render(request, 'sharkommerce/seller_profile.html', context)


def profile(request, user_id):
    user = get_object_or_404(User, pk=user_id)
    if Buyer.objects.filter(user=user).exists():
        return buyer_profile(request, user)
    elif Seller.objects.filter(user=user).exists():
        return seller_profile(request, user)


def seller_review(request, user_id):
    if request.method == "POST":
        seller_review_form = ReviewForm(request.POST)
        if seller_review_form.is_valid():
            data = seller_review_form.cleaned_data
            reviewer = Buyer.objects.get(user_id=request.user.id)
            seller_reviewed = Seller.objects.get(user_id=user_id)
            if reviewer.check_if_bought(seller_reviewed):
                sellerReview = SellerReview(seller= seller_reviewed, buyer= reviewer, date=datetime.date.today(),
                                        vote=data["vote"], title=data["title"], description=data["description"])
                try:
                    sellerReview.save()

                except IntegrityError:
                    messages.error(request,"Errore: recensione già inserita per questo venditore")

            else:
                messages.error(request, 'Errore: non risultano acquisti presso questo venditore')
        else:
            print("e invece no!")

        return HttpResponseRedirect(reverse('sharkommerce:profile', args=[user_id]))


#questa view serve per restituirmi i prodotti nella funziona javascript updateProducts()
def product_cat(request, cat, subcat):
    products = Product.objects.filter(categories__cat_name=cat, categories__subcat_name=subcat)
    #la conversione set/list è perchè, a quanto pare, non c'è modo di fare una distinct sul name dal db.
    return JsonResponse({"products": list(set([prod.name for prod in products]))})


def get_filtered_obj(category, subcategory, max_price, min_vote):
    #min_vote, se l'utente non mette niente, torna None
    #max_price, alla peggio, è già al massimo che può assumere
    #category e subcategory, se l'utente non ha selezionato nulla, è ""

    #Dalla documentazione: https://docs.djangoproject.com/en/3.1/topics/db/queries/#querysets-are-lazy
    #QuerySets are lazy – the act of creating a QuerySet doesn’t involve any database activity.
    # You can stack filters together all day long, and Django won’t actually run the query until the QuerySet is
    # evaluated
    products=Product.objects.all() # di default, torna tutti i prodotti
    if max_price:# ci sarà sempre
        products = products.filter(price__lte=max_price)
    if min_vote: # Se un prodotto non è stato recensito, non rientrerà nei filtri.
        products = products.filter(purchase__productreview__vote__gte=min_vote)
    if category != "":
        products = products.filter(categories__cat_name=category)
    if subcategory != "":
        products = products.filter(categories__subcat_name=subcategory)

    return products


# le funzioni utilizzate che non compaiono in questa classe sono in sharkommerce.Utils.CommonUtils
def products(request):
    filters_form = FiltersForm()
    products=Product.objects.all()
    context = {'filters_form': filters_form, 'products': products}
    if request.method == 'POST':
        filters_form = FiltersForm(request.POST)
        #context.update({'filters_form': filters_form})
        #se la post viene fatta dal bottone dei filtri ( e la form è valida, ma per ora lo sarà sempre, perchè sono tutti e 3 i campi required a false )
        if request.POST.get("filtra") and filters_form.is_valid():
            data = filters_form.cleaned_data
            if check_category_subcategory(data["categories"], data["subcategories"]):
                products = get_filtered_obj(data["categories"], data["subcategories"], data["price"], data["reviews"]) #controlal che category e subcategory siano compatibili
                filters_form = FiltersForm()
                context.update({'filters_form': filters_form, 'products': products})
            else:
                print("Errore nel match tra category e subcategory")

    return render(request, 'sharkommerce/products.html', context)

