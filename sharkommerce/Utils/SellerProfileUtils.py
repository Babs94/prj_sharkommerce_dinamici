import locale
import calendar
from django.db.models import Avg, Count
from django.db.models.functions import ExtractYear, ExtractMonth
from django.db import connection
from sharkommerce.Utils.CommonUtils import check_category_subcategory
from sharkommerce.models import SellerReview, Product, Category

#it protects against modifies of javascript code ( changing subcategories options )
# def check_cat_subcat(cat, subcat):
#     try:
#         Category.objects.get(cat_name=cat, subcat_name=subcat)
#     except Category.DoesNotExist:
#         return False
#     return True

def check_cat_subcat_prod(cat, subcat, prod):
    #if check_cat_subcat(cat, subcat):
    if check_category_subcategory(cat, subcat):
        try:
            Product.objects.get(categories=Category.objects.get(cat_name=cat, subcat_name=subcat), name=prod)
        except Product.DoesNotExist:
            return False
        return True
    else:
        return False


#la newProduct è stata messa non obbligatoria per dare la possibilità di usare la select box dei prodotti, ma se è
# stata selezionata la checkbox, deve essere compilata la relativa form col nome del prodotto.
def check_newProd_compiled(prodName):
    if prodName == "":
        return False
    return True

#DA RIMETTERCI MANO - MEDIA RECENSIONI TOTALE PER L'ANNO. SE CI SONO UTENTI CHE RECENSISCONO DOPO, TIPO IN UN MESE CHE NON E' QUELLO DELL'ACQUISTO, SBALLA.
def get_tot_years (purch_stat_months):
    purch_stat_years = dict.fromkeys(purch_stat_months)
    for year in purch_stat_years.keys():
        tot_revenue = 0
        tot_sales = 0
        tot_reviews = 0
        sum_reviews = 0
        for month in purch_stat_months[year]:
            tot_revenue += purch_stat_months[year][month][0]# primo elemento per il guadagno mensile
            tot_sales += purch_stat_months[year][month][1] #secondo elemento per il numero di vendite
            tot_reviews += purch_stat_months[year][month][3] #quarto elemento per il numero di recensioni
            sum_reviews += purch_stat_months[year][month][4]*purch_stat_months[year][month][3]  # riporto la somma delle recensioni ad avere il punteggio totale corretto, moltiplicando la recensione media del mese * il n di recensioni del mese

        purch_stat_years[year] = [tot_revenue, tot_sales, tot_revenue/tot_sales, tot_reviews, round(sum_reviews/tot_reviews,2) if tot_reviews != 0 else 0]  #terzo elemento per il guadagno medio a vendita

    return purch_stat_years

def get_tot_months(purch_statistics, reviews_statistics):
    years = set(pur["year"] for pur in purch_statistics)
    locale.setlocale(locale.LC_ALL, 'it_IT.utf8')
    #primo elemento della lista per il guadagno mensile, secondo per il numero degli acquisti nel mese, terzo per il guadagno medio per acquisto, quarto per il numero di recensioni e il quinto per la media dei voti
    purch_stat_months = {year: {calendar.month_name[int(month)]: [0, 0, 0, 0, 0] for month in range(1, 13)} for year in years}
    #inserisci i valori dei mesi
    for stat in purch_statistics:
        month_name = calendar.month_name[int(stat["month"])]
        purch_stat_months[stat["year"]][month_name][0] += stat["tot_purc"] #primo elemento della lista per il guadagno del mese
        purch_stat_months[stat["year"]][month_name][1] += stat["n_acquisti"] #secondo elemento della lista per il numero degli acquisti del mese
        purch_stat_months[stat["year"]][month_name][2] += purch_stat_months[stat["year"]][month_name][0] / purch_stat_months[stat["year"]][month_name][1] #terzo elemento della lista per il guadagno medio ad acquisto
    for elem in reviews_statistics:
        purch_stat_months[str(elem["year"])][calendar.month_name[elem["month"]]][3] += elem["n_rev"]
        purch_stat_months[str(elem["year"])][calendar.month_name[elem["month"]]][4] += elem["avg_vote"]
    return purch_stat_months

#prende il guadagno per mese,anno dei venditori
def get_tot_statistics(user):
    with connection.cursor() as cursor:
        cursor.execute(
            'select strftime("%%Y", purchase.date) as year, strftime("%%m", purchase.date) as month, sum(purchase.price*purchase.qty) as tot_purc, count(*) as n_acquisti '
            'from sharkommerce_purchase purchase join sharkommerce_product product on '
            'product.id = purchase.product_id where product.seller_id = %s '
            'group by year, month', [user.id])
        columns = [col[0] for col in cursor.description]

        #https: // www.programiz.com / python - programming / methods / built - in / zip
        #cursor.fetchall() fetches all the rows of a query result. It returns all the rows as a list of tuples.
        return [dict(zip(columns, row)) for row in cursor.fetchall()]



#prende la media per mese,anno delle recesioni dei clienti ai venditori
def get_avg_rev_statistics(user):
    return SellerReview.objects.filter(seller=user.id).annotate(year=ExtractYear('date'), month=ExtractMonth('date')).values('year','month').annotate(avg_vote=Avg('vote'), n_rev=Count('vote')).order_by()

