from sharkommerce.models import Category

def check_category_subcategory (category, subcategory):
    #la if serve perchè la view dei products potrebbe dare elementi vuoti
    if category and subcategory:
        try:
            Category.objects.get(cat_name=category, subcat_name=subcategory)
        except Category.DoesNotExist:
            return False
    return True