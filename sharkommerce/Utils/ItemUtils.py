from django.db import IntegrityError, connection

def get_items_in_same_purchase(item):
    with connection.cursor() as cursor:
        cursor.execute(
            #l'idea è quella di estrarre gli acquisti fatti insieme a quel prodotto ( stesso buyer, stessa data ) e
            #ordinarli per numero di volte in cui compaiono, così da avere quelli acquistati più di frequente insieme
            #all'oggetto considerato.
            'select extracted_purch.product_id, count(extracted_purch.product_id) as times '
            'from sharkommerce_purchase as extracted_purch '
            'join' #acquisti fatti di quel prodotto
                '(select buyer_id, date from sharkommerce_purchase '
                ' where product_id = %s) as purchases '
            'on extracted_purch.buyer_id = purchases.buyer_id and extracted_purch.date = purchases.date '
            'where extracted_purch.product_id != %s '
            'group by extracted_purch.product_id '
            'order by count(extracted_purch.product_id) DESC '
            ' limit 3', [item.id, item.id])

        columns = [col[0] for col in cursor.description]
        return [dict(zip(columns, row)) for row in cursor.fetchall()]

def get_items_same_category_more_bought(item):
    with connection.cursor() as cursor:
        cursor.execute(
            #si prendono gli oggetti più acquistati nello stesso name della category dell'oggetto da considerare; se
            #non ce ne fossero di acquistati, si prenderanno ugualmente tre oggetti di quella category.
            'select prods.id, count(prods.id) as times ' 
            'from sharkommerce_product as prods '
            'join sharkommerce_category as category on prods.categories_id = category.id '
            'left join sharkommerce_purchase as purchs on prods.id = purchs.product_id ' 
            'where category.cat_name=%s and prods.id != %s '
            'group by prods.id '
            'order by count(prods.id) desc '
            'limit 3 ', [item.categories.cat_name, item.id] )

        columns = [col[0] for col in cursor.description]
        return [dict(zip(columns, row)) for row in cursor.fetchall()]