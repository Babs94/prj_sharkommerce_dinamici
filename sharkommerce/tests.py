from django.core.exceptions import ValidationError
from django.test import TestCase
from django.urls import reverse

from sharkommerce.models import *
import tempfile

#test sulla Product
class ProductMethodTests(TestCase):
    def test_decrease_qty_below_zero(self):
        user_seller = User.objects.create_user(username="test_seller", email="testmail2@mail.com",
                                               password="test_seller")
        user_seller.save()
        seller = Seller(user=user_seller, store="store_test",
                        average_rate=6.0)  # se aggiungi il calcolo sulla media, sennò toglilo
        seller.save()
        category = Category(cat_name='crostacei', subcat_name='caridine')
        category.save()
        product = Product(categories=category, seller=seller, name="Red Cherry",
                          image=tempfile.NamedTemporaryFile(suffix=".jpg").name, qty=3, price=3.50,
                          description="Test description")  # può essere debba aggiungere la voce dell'avg rate
        with self.assertRaises(ValueError):
            product.update_qty(4)


    def test_decrease_qty_negative_value(self):
        category = Category(cat_name='crostacei', subcat_name='caridine')
        user_seller = User.objects.create_user(username="test_seller", email="testmail2@mail.com",
                                               password="test_seller")
        seller = Seller(user=user_seller, store="store_test",
                        average_rate=6.0)  # se aggiungi il calcolo sulla media, sennò toglilo
        product = Product(categories=category, seller=seller, name="Red Cherry",
                          image=tempfile.NamedTemporaryFile(suffix=".jpg").name, qty=3, price=3.50,
                          description="Test description")  # può essere debba aggiungere la voce dell'avg rate
        with self.assertRaises(ValueError):
            product.update_qty(-4)

    #vedi se devi testare anche l'aggiunta alla quantità dei prodotti

    def test_product_available_with_qty_zero(self):
        category = Category(cat_name='crostacei', subcat_name='caridine')
        user_seller = User.objects.create_user(username="test_seller", email="testmail2@mail.com",
                                               password="test_seller")
        seller = Seller(user=user_seller, store="store_test",
                        average_rate=6.0)  # se aggiungi il calcolo sulla media, sennò toglilo
        product = Product(categories=category, seller=seller, name="Blue Dream",
                          image=tempfile.NamedTemporaryFile(suffix=".jpg").name, qty=0, price=3.50,
                          description="Test description")  # può essere debba aggiungere la voce dell'avg rate
        self.assertEqual(product.is_available(), False)

class ItemViewTests(TestCase):

    def test_not_existant_profile(self):
        response = self.client.get(reverse('sharkommerce:item', args=[150]))
        self.assertEqual(response.status_code, 404)


    def test_is_recommended_items_three_if_item_never_bought(self):
        category = Category(cat_name='crostacei', subcat_name='caridine')
        category.save()
        user_seller = User.objects.create_user(username="test_seller", email="testmail2@mail.com",
                                               password="test_seller")
        user_seller.save()
        seller = Seller(user=user_seller, store="store_test",
                        average_rate=0)  # se aggiungi il calcolo sulla media, sennò toglilo
        seller.save()
        product = Product(categories=category, seller=seller, name="Blue Dream",
                          image=tempfile.NamedTemporaryFile(suffix=".jpg").name, qty=10, price=3.50,
                          description="Test description")  # può essere debba aggiungere la voce dell'avg rate
        product.save()
        response = self.client.get(reverse('sharkommerce:item', args=[product.id]))
        self.assertEqual(len(response.context['recommended_items']),3)

    def test_is_recommended_items_three_if_item_bought_with_only_one_other_prod(self):
        category = Category(cat_name='crostacei', subcat_name='caridine')
        category.save()
        user_seller = User.objects.create_user(username="test_seller", email="testmail2@mail.com",
                                               password="test_seller")
        user_seller.save()
        seller = Seller(user=user_seller, store="store_test",
                        average_rate=0)  # se aggiungi il calcolo sulla media, sennò toglilo
        seller.save()
        product = Product(categories=category, seller=seller, name="Blue Dream",
                          image=tempfile.NamedTemporaryFile(suffix=".jpg").name, qty=10, price=3.50,
                          description="Test description")  # può essere debba aggiungere la voce dell'avg rate
        product.save()
        #item =
        user_buyer = User.objects.create_user(username="test_buyer", email="testmail@mail.com", password="test_buyer")
        buyer = Buyer(user=user_buyer, address="via immaginaria", birth_date='2021-10-02')
        buyer.save()
        first_purchase = Purchase(product=product, buyer=buyer, qty=3, date='2021-01-15', price=3.50)
        first_purchase.save()


# se si vuole inserire una recensione con voto 0
    # def test_insert_review_with_vote_0(self):
    #     user_seller = User.objects.create_user(username="test_seller", email="testmail2@mail.com", password="test_seller")
    #     user_seller.save()
    #     seller = Seller(user=user_seller, store="store_test", average_rate=6.0)#se aggiungi il calcolo sulla media, sennò toglilo
    #     seller.save()
    #     category = Category(cat_name='crostacei', subcat_name='caridine')
    #     category.save()
    #     product = Product(categories=category, seller=seller, name="Red Cherry",image=tempfile.NamedTemporaryFile(suffix=".jpg").name, qty=30, price=3.50, description="Test description" )#può essere tu debba aggiungere la voce dell'avg rate
    #     product.save()
    #     user_buyer = User.objects.create_user(username="test_buyer", email="testmail@mail.com", password="test_buyer")
    #     buyer = Buyer(user=user_buyer, address="via immaginaria", birth_date='2021-10-02')
    #     buyer.save()
    #     purchase = Purchase(product=product, buyer=buyer, qty=3, date='2021-01-15', price=3.50)
    #     purchase.save()
    #     with self.assertRaises(ValidationError):
    #         review = ProductReview(purchase=purchase, vote=15, title="Title Test", description="Description test",
    #                                date="2021-01-16")
    #         review.full_clean()
    #         review.save()




