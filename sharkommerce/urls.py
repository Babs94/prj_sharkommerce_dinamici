from django.conf.urls import url
from django.urls import path
from django.views import static
from prj_sharkommerce import settings
from . import views

app_name = 'sharkommerce'

urlpatterns=[
    url(r'^$', views.index, name='index'), #così, si apre subito sulla main page, non c'è bisogno di aprire /sharkommerce; è ok?
    path('item/<int:product_id>/', views.item, name='item'),
    url(r'^login/$', views.login_view, name='login'),
    url(r'^register/$', views.register, name='register'),
    url(r'^cart/$', views.cart, name='cart'),
    #view utilizzata per togliere l'elemento dal carrello
    path('cart/remove/<int:product_id>/',views.removeFromCart, name='removeFromCart'),
    url(r'^logout/$', views.logout_view, name='logout'),
    path('profile/<int:user_id>/', views.profile, name='profile'),
    url(r'^profile/(?P<user_id>[0-9]+)/seller_review$', views.seller_review, name='seller_review'),
    url(r'^profile/product_review$', views.product_review, name='product_review'),
    #utilizzato per avere il json dei prodotti in una certa category+subcategory
    path('products/<str:cat>/<str:subcat>/', views.product_cat, name='product_cat'),
    # used to get media files
    url(r'^media/(?P<path>.*)$', static.serve, {'document_root': settings.MEDIA_ROOT}),
    path('products', views.products, name='products'),
    path('purchase', views.purchase, name='purchase')


]