from django.contrib import admin
from .models import *


class RangeVoteFilter(admin.SimpleListFilter):
    title = 'vote'
    parameter_name = 'vote'

    def lookups(self, request, model_admin):
        return (
            ('1', 'sufficient'),
            ('0', 'not sufficient'),
        )

    def queryset(self, request, queryset):
        if self.value() == '0':
            return queryset.filter(vote__lte=5)
        if self.value() == '1':
            return queryset.filter(vote__gt=5)


class SellerReviewAdmin(admin.ModelAdmin):
    fieldsets = [
        ('Store', {'fields':['seller', 'buyer', 'vote','date']}),
        ('Content review',{'fields':['title','description'],'classes':['collapse']})
    ]

    list_display = ['seller','buyer','vote', 'date']
    list_filter = ['seller', RangeVoteFilter]


admin.site.register(SellerReview, SellerReviewAdmin)


class ProductReviewAdmin(admin.ModelAdmin):

    fieldsets = [
        ('Product', {'fields': ['purchase', 'vote', 'date']}),
        ('Content review', {'fields': ['title', 'description'], 'classes': ['collapse']})
    ]


    def purch(self, obj):
        return obj.purchase.product

    def buyer(self,obj):
        return obj.purchase.buyer

    list_display = ['purch','buyer','vote','date']
    list_filter = ['vote']
    search_fields = ['purchase__product__name']


admin.site.register(ProductReview, ProductReviewAdmin)


class ProductAdmin(admin.ModelAdmin):
    fieldsets =[
        ('Product', {'fields': ['name', 'seller']}),
        ('Details', {'fields': ['categories', 'price', 'qty','description'], 'classes': ['collapse']})
    ]

    list_display = ['name','seller']
    list_filter = ['seller']
    search_fields = ['name']


admin.site.register(Product,ProductAdmin)


class PurchaseAdmin(admin.ModelAdmin):
    fieldsets = [
        ('Product', {'fields': ['product', 'buyer','date','qty','price']})
    ]

    list_display = ['product', 'buyer','date','qty','price']
    list_filter = ['buyer','date']


admin.site.register(Purchase, PurchaseAdmin)


class BuyerAdmin(admin.ModelAdmin):
    fieldsets = [
        ('Product', {'fields': ['user', 'address','birth_date']})
    ]

    list_display = ['user', 'address', 'birth_date']
    search_fields = ['user__username']


admin.site.register(Buyer, BuyerAdmin)

class SellerAdmin(admin.ModelAdmin):
    fieldsets = [
        ('Product', {'fields': ['user', 'store']})
    ]

    list_display = ['user', 'store']
    search_fields = ['store']


admin.site.register(Seller, SellerAdmin)



class CategoryAdmin(admin.ModelAdmin):
    fieldsets = [
        ('Category', {'fields': ['cat_name', 'subcat_name']})
    ]

    list_display = ['cat_name', 'subcat_name']
    list_filter = ['cat_name']


admin.site.register(Category, CategoryAdmin)