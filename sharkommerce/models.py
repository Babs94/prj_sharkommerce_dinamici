import os
from django.core.validators import MinValueValidator, MaxValueValidator
from django.db import models
from django.contrib.auth.models import User
from django.utils.crypto import get_random_string


class Buyer(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE, primary_key=True)
    address = models.CharField(max_length=60)
    birth_date = models.DateField()

    # controlla se l'utente ha fatto un acquisto presso il venditore prima di inserire la recensione
    def check_if_bought(self, seller):
        return Purchase.objects.filter(buyer=self, product__seller=seller).exists()

    def __str__(self):
        return self.user.username


class Seller(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE, primary_key=True)
    store = models.CharField(max_length=50)
    average_rate = models.DecimalField(max_digits=4, decimal_places=2, default=0) # con il calcolo, questo record non mi serve

    def __str__(self):
        return self.store

    def calculateAverageRate(self):#vedi cosa deve tornare
        reviews = SellerReview.objects.filter(seller=self)
        if not reviews.exists():
            return 0 #prima c'era un return False
        sum = 0
        for rev in reviews:
            sum += rev.vote
        return round(sum / reviews.count(),2)


#un acquirente può recensire il venditore una sola volta
class SellerReview(models.Model):
    seller = models.ForeignKey(Seller, on_delete=models.CASCADE)
    buyer = models.ForeignKey(Buyer, null=True, on_delete=models.SET_NULL) # ma perchè null=True se è in foreign key?!
    date = models.DateField()
    vote = models.PositiveSmallIntegerField(validators=[MinValueValidator(1), MaxValueValidator(10)])
    title = models.CharField(max_length=100)
    description = models.TextField()

    class Meta:
        unique_together = ("seller", "buyer")


class Category(models.Model):
    cat_name = models.CharField(max_length=100)
    subcat_name = models.CharField(max_length=100)

    def __str__(self):
        return f"{self.cat_name} {self.subcat_name}"

    class Meta:
        unique_together = ("cat_name", "subcat_name")
        verbose_name_plural = "categories"


def get_img_name(instance, *args):
    return os.path.join(instance.seller.user.username, f"img_{get_random_string(length=16)}")


class Product(models.Model):
    seller = models.ForeignKey(Seller, on_delete=models.CASCADE)
    categories = models.ForeignKey(Category, on_delete=models.RESTRICT)
    name = models.CharField(max_length=100)
    image = models.ImageField(blank=False, upload_to=get_img_name)
    qty = models.IntegerField(default=0)
    price = models.DecimalField(max_digits=5, decimal_places=2, default=0.00)
    description = models.TextField()
    #questo deve avere un average rate

    class Meta:
        unique_together = ('id', 'seller')

    def __str__(self):
        return self.name

    #se il seller vuole fare refill dei prodotti?
    def update_qty(self, qty):
        if qty <= self.qty and qty > 0:
            self.qty -= qty
            self.save()
        else:
            raise ValueError("Error during update product's quantity")

    def is_available(self):
        return self.qty > 0


    def calculateAverageRate(self):
        reviews = ProductReview.objects.filter(purchase__product=self)
        if not reviews.exists():
            return "Nessuna recensione inserita"
        sum = 0
        for rev in reviews:
            sum += rev.vote
        return round(sum / reviews.count(), 2)


class Purchase(models.Model):
    buyer = models.ForeignKey(Buyer, on_delete=models.SET_NULL, null=True) #se sono in chiave, non possono essere null
    product = models.ForeignKey(Product, on_delete=models.CASCADE)
    qty = models.IntegerField(default=1)
    date = models.DateField()
    price = models.DecimalField(max_digits=5, decimal_places=2, default=0.00)


    class Meta:
        unique_together = ('product', 'date', 'buyer') #va bene cosi o mi conviene usare i numeri gentilmente offerti da Django? per la migration delle prod rev, viene sfruttato il fatto che siano unique

    def get_tot_purc(self):
        return self.qty

    def __str__(self):
        return f"{self.product.name} {self.buyer} {self.date}"


class ProductReview(models.Model):
    purchase = models.ForeignKey(Purchase, on_delete=models.CASCADE)
    vote = models.PositiveSmallIntegerField(validators=[MinValueValidator(1), MaxValueValidator(10)])
    title = models.CharField(max_length=100)
    description = models.TextField()
    average_rate = models.DecimalField(max_digits=4, decimal_places=2, default=0) #questa va cancellata, non la userai mai, ma per le magie di sqlite è un casino,
    date = models.DateField(null=True)                                                                                #quindi lo faremo col progetto definitivo da subito




