from django import forms
from django.core.validators import MaxValueValidator

from sharkommerce.models import Category, Product
from django.db.models import Max, Min


class LoginForm(forms.Form):
    username = forms.CharField(label='username', widget=forms.TextInput(
        attrs={'class': 'form-control', 'placeholder': 'Insert here your username'}))
    password = forms.CharField(widget=forms.PasswordInput(attrs={'class': 'form-control', 'placeholder': 'Insert '
                                                                                                         'here your '
                                                                                                         'password'}))


class RegisterForm(forms.Form):
    username = forms.CharField(max_length=50, label='Username', widget=forms.TextInput(attrs={'class': 'form-control'}))
    mail = forms.EmailField(label='E-mail', widget=forms.TextInput(attrs={'class': 'form-control'}))
    password = forms.CharField(label='Password', widget=forms.PasswordInput(attrs={'class': 'form-control'}))
    control_password = forms.CharField(label='Password', widget=forms.PasswordInput(attrs={'class': 'form-control'}))
    REGISTER_CHOICES = [('Acquirente', 'Acquirente'), ('Venditore', 'Venditore')]
    tipo = forms.ChoiceField(label='Tipo', choices=REGISTER_CHOICES, initial='Acquirente', widget=forms.RadioSelect())
    negozio = forms.CharField(max_length=50, label='Nome negozio',
                              widget=forms.TextInput(attrs={'class': 'form-control'}),
                              required=False)
    DATE_INPUT_FORMATS = ['%d-%m-%Y']
    data_nascita = forms.DateField(label="data_nascita", widget=forms.TextInput(attrs={'class': 'form-control',
    'placeholder': 'DD-MM-YYYY'}), input_formats= DATE_INPUT_FORMATS, required=False)
    indirizzo = forms.CharField(max_length=60, label='Indirizzo',            #migliorabile
                                widget=forms.TextInput(attrs={'class': 'form-control'}), required=False)

    def clean(self):
        cleaned_data = super(RegisterForm, self).clean()
        password = cleaned_data.get("password")
        control_password = cleaned_data.get("control_password")

        if password != control_password:
            raise forms.ValidationError("Password and confirmation must match", code="password")


class ProductForm(forms.Form):
    image = forms.ImageField(widget=forms.FileInput())# se ti annoi, customizza il bottone
    CATEGORY_CHOICES = set([(cat.cat_name, cat.cat_name) for cat in Category.objects.all()])
    SUBCATEGORY_CHOICES = set([(cat.subcat_name, cat.subcat_name) for cat in Category.objects.all()])
    PRODUCT_CHOICHES = [(prod.categories, prod.name) for prod in Product.objects.all()]
    category = forms.ChoiceField(choices=CATEGORY_CHOICES, widget=forms.Select(attrs={'class': 'custom-select','id': 'category-box', 'onchange': 'updateSubcats()'}))
    subcategory = forms.ChoiceField(choices=SUBCATEGORY_CHOICES, widget=forms.Select(attrs={'class': 'custom-select', 'id': 'subcat_box', 'onchange': 'updateProducts()'}))
    product = forms.ChoiceField(choices=PRODUCT_CHOICHES, widget=forms.Select(attrs={'id':'id_product','class':'custom-select'}), required=False)
    check_new_product = forms.BooleanField(label="Nuovo prodotto", initial=False, widget=forms.CheckboxInput(attrs={'onclick':'activateNewProd()'}), required=False )
    new_product = forms.CharField( widget=forms.TextInput(attrs={'class': 'form-control', 'disabled':'True'}),required=False)
    price = forms.DecimalField(label="Prezzo cad.", min_value=0.00, decimal_places=2, initial=0.00, widget=forms.NumberInput(attrs={'class': 'form-control'}))
    quantity = forms.IntegerField(label="Q.ta", min_value=1, initial=1, widget=forms.NumberInput(attrs={'class': 'form-control'}))
    description = forms.CharField(label="Descrizione", widget=forms.Textarea(attrs={"class": "form-control", "rows": 3, "spellcheck": False}))


class ReviewForm(forms.Form):
    vote = forms.IntegerField(label="Voto:", min_value=1, max_value=10, widget=forms.NumberInput(attrs={'class': 'form-control'}))
    title = forms.CharField(label="Titolo:", widget=forms.TextInput(attrs={'class':'form-control col'}))
    description = forms.CharField(label="Descrizione:", widget=forms.Textarea(attrs={'class': 'form-control col', 'rows': '3'}))

class ProductReviewForm(ReviewForm):
    purch_id = forms.IntegerField(widget=forms.NumberInput(attrs={'style':'display: none','id':'purch_id_review' }))


class FiltersForm(forms.Form):
    CATEGORIES_CHOICES = set([(cat.cat_name, cat.cat_name) for cat in Category.objects.all()])
    CATEGORIES_CHOICES = [('','')] + list(CATEGORIES_CHOICES)
    categories = forms.ChoiceField(label="Categorie",choices=CATEGORIES_CHOICES, widget=forms.Select(attrs={'class':'custom-select', 'onchange':'category_filter_changed()', 'id':'cat_filter'}), required=False)
    SUBCATEGORIES_CHOICES = set([(cat.subcat_name, cat.subcat_name) for cat in Category.objects.all()])
    SUBCATEGORIES_CHOICES = [('', '')] + list(SUBCATEGORIES_CHOICES)
    subcategories = forms.ChoiceField(label="Sottocategorie", choices=SUBCATEGORIES_CHOICES, widget=forms.Select(attrs={'class':'custom-select', 'id':'subcat_filter'}), required=False)
    MAX_PRICE = Product.objects.aggregate(Max("price")) #questo potrebbe essere un problema se il db è vuoto?
    MAX_PRICE = MAX_PRICE["price__max"]
    MIN_PRICE = Product.objects.aggregate(Min("price"))
    MIN_PRICE = MIN_PRICE["price__min"]
    price = forms.DecimalField(decimal_places=2, widget=forms.NumberInput(attrs={'type':'range','class':'custom-range', 'min': MIN_PRICE, 'max': MAX_PRICE,'id':'myRange'}), required=False) #gli attrs valgono da validators? si direbbe di sì
    reviews =forms.IntegerField(widget=forms.NumberInput(attrs={'style':'display: none','id':'avgVoteMin', 'min': 1, 'max':10 }), required=False)

class ItemForm(forms.Form):
    qty = forms.IntegerField(label="Q.ta", min_value=1, initial=1, widget=forms.NumberInput(attrs={'class': 'form-control'}))#controlla che non prenda 0 dal js

    def __init__(self, *args, **kwargs):
        self.max_qty = kwargs.pop('max_qty', None)
        super().__init__(*args, **kwargs)
        self.fields["qty"].widget.attrs["max"] = self.max_qty
        self.fields["qty"].max_value = self.max_qty
        self.fields["qty"].validators.append(MaxValueValidator(self.max_qty))

